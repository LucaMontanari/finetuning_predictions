from tensorflow.python.lib.io import file_io
from tensorflow.contrib import predictor
from PIL import Image

import prediction_utils as pred_util
import sklearn.metrics as sm
import numpy as np
#import cStringIO
import base64
import sys
import os
import io

sys.path.append( os.path.join(os.path.dirname(os.path.abspath(__file__)),'..','tf-hub-images') )
from tf_hub_images.utils.image import read_image_file_bytes_in_jpeg


def open_file(uri, mode="r" ,*args, **kwargs):
    if uri.startswith("gs://"):
        return file_io.FileIO(uri, mode=mode)
    else:
        return open(uri, mode, *args, **kwargs)

"""def gcs_image_b64(uri):
    with open_file(uri, 'rb') as f:
        image_bytes = f.read()
        img = Image.open(io.BytesIO(image_bytes)).convert('RGB')
        buffer = cStringIO.StringIO()
        img.save(buffer, format="JPEG")
        img_str = base64.b64encode(buffer.getvalue())
        return img_str"""

def run():
    with open_file("gs://ml-research-injenia/flowers/dataset/csv/classes.csv", 'r') as f:
        labels = f.read().replace("\r","").strip().split("\n")

    labels = [l.strip('"') for l in labels]

    with open_file("gs://ml-research-injenia/flowers/dataset/csv/eval_set.csv", 'r') as f:
        lines = f.read().strip().split("\n")[1:]
        content = [l.strip().split(",") for l in lines]
        content = [[ ",".join(c[:-1]), c[-1]] for c in content]

    predict_fn = predictor.from_saved_model("gs://ml-research-injenia/flowers/nets/fine_tuning/mobilenet_v2_140_224/feature_vector/2/training/flowers_labels/export/bytestring_images/1552495323")

    results = []

    for i, c in enumerate(content):
        
        print(str(i+1)+"/"+str(len(content)))
        image_uri = c[0]
        expected_label = c[1]
        expected_prediction = labels.index(expected_label)

        image_uri = c[0].strip('"')
        expected_label = c[1].strip('"')

        if(expected_label in labels):
            expected_prediction = labels.index(expected_label)
        else:
            expected_prediction = len(labels)

        try:
            #image_bytes = gcs_image_b64(image_uri)
            image_bytes = read_image_file_bytes_in_jpeg(image_uri)
        except:
            print("ERROR: unable to correctly open:\n"+image_uri)
            continue

        #prediction = predict_fn({"images_bytes":[image_bytes.decode('base64')],"keys":["0"]})
        prediction = predict_fn({"images_bytes":[image_bytes],"keys":["0"]})
        int_label = prediction["predictions"][0]
        print(labels[int_label] + "     -     " + expected_label)
        print(prediction)

        result = {
            "image_uri" : image_uri,
            "expected_label" : expected_label,
            "expected_prediction" : expected_prediction,
            "net_out" : prediction
        }
        results.append(result)

    print("")
    print("gathering statistics..")

    total = 0.0
    correct = 0.0
    correct_results = []
    y_test = []
    y_pred = []
    y_test_txt = []
    y_pred_txt = []
    y_out = []
    successes=[]
    for r in results:
        total += 1
        expected_prediction = r["expected_prediction"]
        prediction = r["net_out"]["predictions"][0]
        out = np.around(r["net_out"]["scores"])
        y_test.append(expected_prediction)
        y_pred.append(prediction)
        successes.append(expected_prediction==prediction)
        if(expected_prediction < len(labels)):
            y_test_txt.append(labels[expected_prediction])
        else:
            y_test_txt.append('n/a')

        if(prediction < len(labels)):
            y_pred_txt.append(labels[prediction])
        else:
            y_pred_txt.append('n/a')

        y_out.append(out)
        if expected_prediction == prediction:
            correct += 1
            correct_results.append(r)

    y_out = np.array(y_out)

    label_idxs = range(len(labels))
    cm = sm.confusion_matrix(y_pred=y_pred, y_true=y_test, labels=label_idxs)

    y_out = pred_util.one_hot(y_pred, len(labels))

    with open_file('confusion_matrix.png', 'wb') as of:
        pred_util.plot_confusion_matrix(cm, labels, output=of)

    #with open_file('confusion_matrix_normalized.png', 'wb') as of:
    #    pred_util.plot_confusion_matrix(cm, labels, output=of, normalize=True, title='Normalized Confusion Matrix')


if __name__ == '__main__':
    run()
