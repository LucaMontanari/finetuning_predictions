import numpy as np
import itertools

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues,
                          output=None):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        #print("Normalized confusion matrix")
    #else:
        #print('Confusion matrix, without normalization')

    #print(cm)
    plt.figure(figsize=(12,12))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=90)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    if output != None:
        plt.savefig(output)
        plt.clf()


def print_cm_file(output, cm, labels, hide_zeroes=False, hide_diagonal=False, hide_threshold=None):
    """pretty print for confusion matrixes"""
    o = output
    columnwidth = max([len(x) for x in labels]+[5]) # 5 is value length
    empty_cell = " " * columnwidth
    # Print header
    o.write("    " + empty_cell + ' ')
    for label in labels:
        o.write(("%{0}s".format(columnwidth) % label) + ' ')
    o.write('\n')
    # Print rows
    for i, label1 in enumerate(labels):
        o.write(("    %{0}s".format(columnwidth) % label1) + ' ')
        for j in range(len(labels)):
            cell = "%{0}.1f".format(columnwidth) % cm[i, j]
            if hide_zeroes:
                cell = cell if float(cm[i, j]) != 0 else empty_cell
            if hide_diagonal:
                cell = cell if i != j else empty_cell
            if hide_threshold:
                cell = cell if cm[i, j] > hide_threshold else empty_cell
            o.write(cell + ' ')
        o.write('\n')

def one_hot(labels, n_labels):
    l = len(labels)
    oh = np.zeros((l, n_labels))
    oh[np.arange(l), labels] = 1
    return oh

def get_iou(bb1, bb2):
    """
    Calculate the Intersection over Union (IoU) of two bounding boxes.

    Parameters
    ----------
    bb1 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x1, y1) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner
    bb2 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x, y) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner

    Returns
    -------
    float
        in [0, 1]
    """
    assert bb1['x1'] < bb1['x2']
    assert bb1['y1'] < bb1['y2']
    assert bb2['x1'] < bb2['x2']
    assert bb2['y1'] < bb2['y2']

    # determine the coordinates of the intersection rectangle
    x_left = max(bb1['x1'], bb2['x1'])
    y_top = max(bb1['y1'], bb2['y1'])
    x_right = min(bb1['x2'], bb2['x2'])
    y_bottom = min(bb1['y2'], bb2['y2'])

    if x_right < x_left or y_bottom < y_top:
        return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
    bb1_area = (bb1['x2'] - bb1['x1']) * (bb1['y2'] - bb1['y1'])
    bb2_area = (bb2['x2'] - bb2['x1']) * (bb2['y2'] - bb2['y1'])

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
    assert iou >= 0.0
    assert iou <= 1.0
    return iou

def intersection_over_union(predictions, labels_one_hotted):
    """
    Performs intersection over union

    Args:
        - predictions: a numpy array with shape (batch_size, n_labels)
        - labels_one_hotted: numpy array with shape (batch_size, n_labels)
    Returns:
        - n right predictions per class
    """
    intersection = predictions * labels_one_hotted
    i_sum = np.sum(intersection, axis=0)
    union = predictions + labels_one_hotted
    den = np.sum(union, axis=0)
    return 2*i_sum.astype(float)/den.astype(float)

def intersection_over_union_sum(predictions, labels_one_hotted):
    """
    Performs intersection over union overall

    Args
        - predictions: a numpy array with shape (batch_size, n_labels)
        - labels_one_hotted: numpy array with shape (batch_size, n_labels)
    Returns
        - Sum of all the right predictions divided by the union of both the prediction and
    """
    intersection = predictions * labels_one_hotted
    i_sum = np.sum(intersection)
    union = predictions + labels_one_hotted
    den = np.sum(union)
    return 2*i_sum.astype(float)/den.astype(float)

def string_iou(iou, label_names):
    return '\n'.join('\t{:25} {:6.3f}'.format(n,i*100) for n,i in zip(label_names, iou))+'\n'
    